// FUNCTIONS TO MOVE WITH THE KEY EVENT :

// Perso move
function moveRight(){
    myPerso.style.transitionDuration = '0.3s';
    myPerso.style.left = 400 + 'px';
}
function returnRight() {
    myPerso.style.left = 200 + 'px';
}


// Rival move
function moveLeft(){
    myRival.style.transitionDuration = '0.3s';
    myRival.style.left = 750 + 'px';
}
function returnLeft(){
    myRival.style.left = 990 + 'px';
}

// To simplify players movements
function persoMove(){
    setTimeout(function(){moveRight();}, 100);
    setTimeout(function(){returnRight()}, 500);
}
function rivalMove(){
    setTimeout(function(){moveLeft()}, 700);
    setTimeout(function(){returnLeft()}, 1100);
}

// FUNCTIONS TO MOVE END 

// FUNCTIONS SPECIAL VELOCIRAPTOR :

// All functions in one function
function veloMode(){
    if(persoChoice === velo && rivalChoice !== velo){
        setTimeout(function(){veloMsg()}, 100); // veloMsg() display 'block' arrows images. The function is in dino_game_functions.js 
        setTimeout(function(){veloMove()}, 100); // veloMove() Velociraptor movements. It is in dino_game_functions.js 
        setTimeout(function(){velo.veloAttack(rivalPower)}, 500); // attack() is a dino class method. It is in dino_class.js 
    } else if(rivalChoice === velo && persoChoice !== velo){
        setTimeout(function(){veloMsg()}, 100); // Same thing 
        setTimeout(function(){veloMove()},100); //
        setTimeout(function(){velo.veloAttack(persoPower)}, 500); //
    } else if(persoChoice === velo && persoChoice === velo){
        setTimeout(function(){veloMsg()}, 100); //
        setTimeout(function(){veloMove()}); //
        setTimeout(function(){velo.veloAttack(rivalPower)}, 100); //
        setTimeout(function(){velo.veloAttack(persoPower)}, 1000); //
    }
}

// To display 'block' images of arrows to show that the velociraptor is faster.
function veloMsg(){
    if(persoChoice === velo && rivalChoice !== velo){
        setTimeout(function(){msgVelo.style.display = 'block'}, 100);
        setTimeout(function(){msgVelo.style.display = 'none'}, 1200);
    }else if(rivalChoice === velo && persoChoice !== velo){
        setTimeout(function(){msgVeloRival.style.display = 'block'}, 100);
        setTimeout(function(){msgVeloRival.style.display = 'none'}, 1200);
    }else if(rivalChoice === velo && persoChoice === velo){
        setTimeout(function(){msgVelo.style.display = 'block'}, 100);
        setTimeout(function(){msgVelo.style.display = 'none'}, 1200);

        setTimeout(function(){msgVeloRival.style.display = 'block'}, 100);
        setTimeout(function(){msgVeloRival.style.display = 'none'}, 1200);
    }
}

// To move Rival velociraptor 
function veloRivalMove(){
    setTimeout(function(){moveLeft()}, 100);
    setTimeout(function(){returnLeft()}, 500);
}
// To simplify all velociraptors movements
function veloMove(){
    if(persoChoice === velo && rivalChoice !== velo){
        persoMove();
    } else if(rivalChoice === velo && persoChoice !== velo){
        veloRivalMove();
    }else if(persoChoice === velo && rivalChoice === velo){
        persoMove();
        setTimeout(function(){veloRivalMove()}, 700);
    }
}

// FUNCTIONS SPECIAL VELOCIRAPTOR END

// SUPER ATTACK IMAGE

// To display block or none superAttack image
function superAttackImg(){ 
    let supAttack = document.querySelector('.supAttack'); // super attack image

    if(persoPower.value < 15000 && persoPower.value > 12000){
        supAttack.style.display = 'block'; 
    } 
    if(persoPower.value <= 12000 || rivalPower.value <= 0|| persoPower.value <= 0){  
        supAttack.style.display = 'none';         
    }
}

// SUPER ATTACK END

// FINALS FUNCTIONS :

// Function to display 'block' a message when the game is over.
function message(){ 
    let youWin = document.querySelector('.youWin'); 
    let youLoose = document.querySelector('.youLoose');

    if(rivalPower.value <= 0){
        youWin.style.display = 'block';
    }else if(persoPower.value <= 0){
        youLoose.style.display = 'block';
    }
}
// To display 'none' the players when the game is over.
function displayNone(){
    if(persoPower.value <= 0 || rivalPower.value <= 0){
        myPerso.style.display = 'none';  
        myRival.style.display = 'none';      

        persoPower.style.display = 'none';
        rivalPower.style.display = 'none';
    }
}

// FINALS FUNCTIONS END 


